from pytools4dart/miniconda-build
RUN mamba install -n ptdvenv notebook jupyterlab \
    && mamba clean -t -y \
    && echo "conda activate ptdvenv" >> /root/.bashrc \
    && echo "conda activate ptdvenv" >> /etc/skel/.bashrc
ENV HOME=/tmp
# create user with a home directory
# ARG NB_USER
# ARG NB_UID
# ENV USER ${NB_USER}
# ENV HOME /home/${NB_USER}
# SHELL ["/bin/bash", "--login", "-c"]
# RUN adduser --disabled-password \
#     --gecos "Default user" \
#     --uid ${NB_UID} \
#     ${NB_USER}
# WORKDIR ${HOME}
# USER ${USER}
# from condaforge/mambaforge:latest
# add https://gitlab.com/pytools4dart/pytools4dart/-/raw/gitlab-ci3/environment.yml?inline=false /tmp/environment.yml
# env TZ=Europe/Paris
# run ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
# run apt-get update \
#     && apt-get install -y gcc g++ git unzip libgtk2.0-0 libsm6 libxft2 curl \
#     && apt-get clean \
#     && mamba env create -n ptdvenv -f /tmp/environment.yml \
#     && conda env list \
#     && sed -i "s/base/ptdvenv/" /etc/skel/.bashrc \
#     && sed -i "s/base/ptdvenv/" ~/.bashrc \
#     && conda clean -t -y
# FROM mambaorg/micromamba
# COPY --chown=$MAMBA_USER:$MAMBA_USER environment.yml /tmp/environment.yml
# RUN micromamba create -f /tmp/environment.yml \
#   && micromamba clean --all --yes
